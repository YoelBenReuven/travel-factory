import {  useCallback, useRef, useEffect } from 'react';





export const useHttpHook = () => {



  const activeHttpRequests = useRef([]);





  useEffect(() => {
    return () => {
      activeHttpRequests.current.forEach(abortCtrl => abortCtrl.abort());
    };
  }, []);



 
  const sendRequest = useCallback(

    async (url, method = 'GET', body = null, headers = {},) => {
      const httpAbortCtrl = new AbortController();
      activeHttpRequests.current.push(httpAbortCtrl);


      try {
        const response = await fetch(url, {
          method,
          body,
          headers,
          signal: httpAbortCtrl.signal
        });
        const responseData = response.json();

        activeHttpRequests.current = activeHttpRequests.current.filter(

          reqCtrl => reqCtrl !== httpAbortCtrl
        );

        if (!response.ok) {

          throw new Error(responseData.message);
        }


        return responseData;
      } catch (err) {

        throw err;
      }
    },
    []
  );



  return { sendRequest };
};


