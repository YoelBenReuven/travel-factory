import { useState } from 'react';
import contactA from '../../assets/images/alex jonathan.jpg';
import contactB from '../../assets/images/janeth carton.jpg';
import contactC from '../../assets/images/john-smith.jpg';
import contactD from '../../assets/images/michael zimber.jpg';


export const useContacts = () => {
    const [contacts, setContacts] = useState(
        [
            { id: 1,  name: { value: "John Smith", isValid: true }, adress: { value: "Riviera State 32/106", isValid: true }, phoneNumber:{ value: "052415877", isValid: true }, image:{ value: contactA, isValid: true }, duty: { value: "Grphic designer", isValid: true }, company:{ value: "TWITTER", isValid: true }, companyAdress: { value:"1355 Market Street Suite 900 San Francisco, CA 94103 United States" , isValid: true }},
            { id: 2,  name: { value: "SHOSHO", isValid: true }, adress: { value: "Jerusalem", isValid: true }, phoneNumber:{ value: "052415877", isValid: true }, image:{ value: contactB, isValid: true }, duty: { value: "Grphic designer", isValid: true }, company:{ value: "TWITTER", isValid: true }, companyAdress: { value:"1355 Market Street Suite 900 San Francisco, CA 94103 United States" , isValid: true }},
            { id: 3,  name: { value: "SHOSHO", isValid: true }, adress: { value: "Jerusalem", isValid: true }, phoneNumber:{ value: "052415877", isValid: true },image:{ value: contactC, isValid: true }, duty: { value: "Grphic designer", isValid: true }, company:{ value: "TWITTER", isValid: true }, companyAdress: { value:"1355 Market Street Suite 900 San Francisco, CA 94103 United States" , isValid: true }},
            { id: 4,  name: { value: "SHOSHO", isValid: true }, adress: { value: "Jerusalem", isValid: true }, phoneNumber:{ value: "052415877", isValid: true }, image:{ value: contactD, isValid: true }, duty: { value: "Grphic designer", isValid: true }, company:{ value: "TWITTER", isValid: true }, companyAdress: { value:"1355 Market Street Suite 900 San Francisco, CA 94103 United States" , isValid: true }},
            

        ]
    )
    const [change,setChange]=useState(false);

    const addContact = (contact) => {
        setContacts([...contacts,contact])

    }
    const updateContact = (contact) => {
      
     let excludedArr=contacts.filter(item=>{
         return item.id !== contact.id
     })
      setContacts([...excludedArr,contact]);

    }

    const deleteContact=(contact)=>{
     debugger;
      let arr= contacts.filter(item=>{
           return item.id !== contact.id
       })
       setContacts(arr);
    }
    return {contacts,change,addContact,updateContact,deleteContact}

}

