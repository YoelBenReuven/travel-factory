const VALIDATOR_TYPE_REQUIRE = 'REQUIRE';
const VALIDATOR_TYPE_PHONE_NUM = "PHONE"

export const VALIDATOR_REQUIRE = () => ({ type: VALIDATOR_TYPE_REQUIRE });
export const VALIDATOR_PHONE_NUM = () => ({ type: VALIDATOR_TYPE_PHONE_NUM });

export const validate = (value, validators) => {
   
    let isValid = true;
    for (const validator of validators) {
        if (validator.type === VALIDATOR_TYPE_REQUIRE) {

            isValid = isValid && value.trim().length > 0;

        }
        else if (validator.type === VALIDATOR_TYPE_PHONE_NUM) {

            var phoneno = /^\d{10}$/;
            isValid =  Boolean(value.match(phoneno)) ? isValid :false ;
        }



    }
    return isValid;
};
