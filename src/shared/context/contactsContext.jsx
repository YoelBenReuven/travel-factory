import React from "react";

const ContactsContext = React.createContext(undefined);

function ContactsProvider({ children, value }) { 
  return (
    <ContactsContext.Provider value={value}>{children}</ContactsContext.Provider>
  );
}

function useCotactsContext() {
  const context = React.useContext(ContactsContext);
  if (context === undefined) {
    throw new Error("useCounterContext must be used within a CounterProvider");
  }
  return context;
}

export { ContactsProvider, useCotactsContext };
