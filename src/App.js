
import './App.css';
import React from 'react';
import {BrowserRouter, Redirect, Route} from "react-router-dom";
import Contacts from './pages/contacts';
import EditContact from './pages/editContact';
import {ContactsProvider} from './shared/context/contactsContext';
import {useContacts} from './shared/hooks/useContacts';
import AddContact from './components/logic/addContact';


function App() {

  const {contacts,change,addContact,updateContact,deleteContact}=useContacts();
  
  return (
    <ContactsProvider value={{contacts,addContact,updateContact,deleteContact}}>
      <BrowserRouter>
        <Route path="/" exact>
          <Redirect to="/contacts" />
        </Route>
        <Route path="/contacts">
          <Contacts></Contacts>
        </Route>
        <Route path="/edit">
          <EditContact></EditContact>
        </Route>
        <Route path="/AddContact">
          <AddContact></AddContact>
        </Route>
      </BrowserRouter>
    </ContactsProvider>
  );
}

export default App;
