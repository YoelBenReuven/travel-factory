import React, { useReducer } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";


import { useCotactsContext } from '../../shared/context/contactsContext';
import contactA from '../../assets/images/alex jonathan.jpg';
import {
    VALIDATOR_REQUIRE,
    VALIDATOR_PHONE_NUM,
    validate,
} from '../../shared/utils/validators';



const TEXT_FIELD_CHANGED = "TEXT_FIELD_CHANGED";
const PHONE_FIELD_CHANGED = "PHONE_FIELD_CHANGED";



const reducer = (state, action) => {
   
    let prop = action.payload.ref;
    let formIsValid = true;
    switch (action.type) {
     
        case TEXT_FIELD_CHANGED:
            for (const [key, value] of Object.entries(state)) {
                if(key == action.payload.ref){
                    formIsValid = formIsValid &&  validate((action.payload.value), [VALIDATOR_REQUIRE()]) 
                }
                if(key != "id" && key != "formIsValid")
                {
                    formIsValid = formIsValid && value.isValid 
                }
                
              }
            return {
                ...state,
                [prop]: { value: action.payload.value, isValid: validate((action.payload.value), [VALIDATOR_REQUIRE()]) },
                formIsValid:formIsValid
            }

        case PHONE_FIELD_CHANGED:
            return {
                ...state,
                phoneNumber: { value: action.payload.value, isValid: validate((action.payload.value), [VALIDATOR_PHONE_NUM()]) },
                formIsValid: { isValid:formIsValid &&  validate((action.payload.value), [VALIDATOR_PHONE_NUM()]) }
            }



    }
}


const AddContact = (props) => {
    let history = useHistory();
    const { addContact } = useCotactsContext();

    const [state, dispatch] = useReducer(reducer,
        {

            id:Math.random()*101|0,
            name: { value: "", isValid: false },
            adress: { value: "", isValid: false },
            company: { value: "", isValid: false },
            companyAdress: { value: "", isValid: false },
            duty: { value: "", isValid: false },
            phoneNumber: { value: "", isValid: false },
            image: { value: contactA, isValid: true },
            formIsValid: { isValid: false }
        });

  




    const OnAddContact = (e) => {
     
        e.preventDefault();
        addContact(state);
        history.push("/contacts");

    }




    return (

        <React.Fragment>

            <StyledForm onSubmit={OnAddContact}>
                <StyledFormImg src={contactA} />
              
                <input value={state.name.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "name" } })} placeholder="contact name"></input>
                <input value={state.adress.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "adress" } })} placeholder="Adrees"></input>
                <input value={state.company.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "company" } })} placeholder="Company"></input>
                <input value={state.companyAdress.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "companyAdress" } })} placeholder="Company Adress"></input>
                <input value={state.duty.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "duty" } })} placeholder="Duty"></input>
                <input value={state.phoneNumber.value} onChange={(e) => dispatch({ type: PHONE_FIELD_CHANGED, payload: { value: e.target.value, ref: "phoneNumber" } })} placeholder="Phone Number"></input>

                <StyledButton disabled={!state.formIsValid.isValid}>Add Contact</StyledButton>
            </StyledForm>

        </React.Fragment>
    );



}

const StyledButton = styled.button`
    background-color: ${({ disabled }) => (disabled ? "red" : "blue")};
    border-radius:5px;
    height:30px;
    margin-top:10px;
    color:white;
   
`;
const StyledForm = styled.form`
width:50%;
margin: auto;
display: flex;
flex-direction: column;
`;
const StyledFormImg = styled.img`
width:20%;
margin: auto;
display: flex;
flex-direction: column;
`;
export default AddContact