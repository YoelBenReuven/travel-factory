import 'font-awesome/css/font-awesome.min.css';

import CardStyle from "../ui/card/card";
import { useHistory } from "react-router-dom";




const Contact = (props) => {

    let history = useHistory();
    let users = props.data;

    const addUser = () => {
        history.push("./AddContact");
    }

    let html;
    if (users) {
        html = users.map((contact, idx) => {

            return (
                idx < users.length - 1
                    ? <CardStyle editContact={() => { props.editContact(contact) }} data={contact}></CardStyle> :
                    <><CardStyle data={contact}></CardStyle>
                        <div  onClick={addUser} style={{width:'10%',margin:'10px',padding:'10px',height:'187px', cursor: 'pointer', width: '150px', backgroundColor: 'white', textAlign: 'center', position: 'relative' }}>
                            <i style={{ fontSize: '50px', position: 'absolute', left: '35%', top: '35%' }} className="fa fa-plus-circle"></i></div></>
            )
        })

    }


    return (
        <>
            {html != null &&
                (
                    html
                )
            }

        </>
    )
}
export default Contact;