import React,{useEffect, useState} from 'react';
import {
    BrowserRouter as Router,
    Link
  } from "react-router-dom";
import 'font-awesome/css/font-awesome.min.css';

import styles from './card.module.css';

import { useCotactsContext } from '../../../shared/context/contactsContext';
import {useHttpHook} from '../../../shared/hooks/httpHook';





const CardStyle = (props) => {


    const { deleteContact } = useCotactsContext();
    const { sendRequest } = useHttpHook();
    const[coords,setCords]=useState({lat:0,long:0});

    
    
    useEffect(()=>{

        try {
            const calcCoords = async () => {
                
                const responseData = await sendRequest(
                    `https://maps.googleapis.com/maps/api/geocode/json?address=${props.data.adress.value}&key=AIzaSyDn7gP0e-LPBh2uJu_0m3UUvxCgzI-HZXs`,
                    'GET',
                    null
                );
                if (responseData.results.length > 0) {
                    setCords({ lat: responseData.results[0].geometry.location.lat, long: responseData.results[0].geometry.location.long });
                }

            }
            calcCoords()


        }
        catch(e){
            console.log(e);
        }

   
          
    },[])



    let contact = props.data;

    return (
        
        <div className={styles.wrapper}>
            <div>
                 <img className={styles.imageProfile} alt="Remy Sharp" src={contact.image.value} />
                <div>{contact.duty.value}</div> 
            </div>
              
                <div className={styles.details}>
                    <div className={styles.contactName}>{contact.name.value}</div>                  
                    <div className={styles.adress}>  <span className={styles.pin}><i class="fa fa-map-marker"></i></span>{contact.adress.value}</div>
                    <div>{coords.long},{coords.lat}</div>
                    <div className={styles.company}>{contact.company.value}</div>
                    <div className={styles.companyAdress}>{contact.companyAdress.value}</div>
                    <div className={styles.phoneNumber}><span className={styles.phonePref}>P:</span>{contact.phoneNumber.value}</div>
                </div>
            <div className={styles.editZone}>
                <Link 
                    to={{
                        pathname: "/edit",
                        state: { contactDetails: contact }
                    }}>
                    <i className={`${styles.pencil} fa fa-edit`}></i>
                </Link>
                <span onClick={()=>deleteContact(contact)} >  <i className={`${styles.trash} fa fa-trash`} ></i></span>
            </div>
         
          
        </div>


    )




}
export default CardStyle;
