import React, { useReducer } from "react";
import { useHistory } from "react-router-dom";
import styled from "styled-components";


import { useCotactsContext } from '../shared/context/contactsContext';


import {
    VALIDATOR_REQUIRE,
    VALIDATOR_PHONE_NUM,
    validate,
} from '../shared/utils/validators';



const TEXT_FIELD_CHANGED = "TEXT_FIELD_CHANGED";
const PHONE_FIELD_CHANGED = "PHONE_FIELD_CHANGED";



const reducer = (state, action) => {

    let prop = action.payload.ref;
    switch (action.type) {
        case TEXT_FIELD_CHANGED:
            return {
                ...state,
                [prop]: { value: action.payload.value, isValid: validate((action.payload.value), [VALIDATOR_REQUIRE()]) },
                formIsValid: { isValid: validate((action.payload.value), [VALIDATOR_REQUIRE()]) }
            }

        case PHONE_FIELD_CHANGED:
            return {
                ...state,
                phoneNumber: { value: action.payload.value, isValid: validate((action.payload.value), [VALIDATOR_PHONE_NUM()]) },
                formIsValid: { isValid: validate((action.payload.value), [VALIDATOR_PHONE_NUM()]) }
            }



    }
}


const EditContact = (props) => {
    let history = useHistory();
    let contact = history.location.state.contactDetails;
    const [state, dispatch] = useReducer(reducer,
        {
            ...history.location.state.contactDetails,
            // name: { value: contact.name, isValid: true },
            // adress: { value: contact.adress, isValid: true },
            // company: { value: contact.company, isValid: true },
            // companyAdress: { value: contact.companyAdress, isValid: true },
            // duty: { value: contact.duty, isValid: true },
            // phoneNumber: { value: contact.phoneNumber, isValid: true },
            // image: { value: contact.image, isValid: true },
            formIsValid: { isValid: true }
        });

    const { updateContact } = useCotactsContext();




    const OnupdateContact = (e) => {
     
        e.preventDefault();
        updateContact(state);
        history.push("/contacts");

    }




    return (

        <React.Fragment>

            <StyledForm onSubmit={OnupdateContact}>
                <StyledFormImg src={state.image.value} />
              
                <input value={state.name.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "name" } })} placeholder="contact name"></input>
                <input value={state.adress.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "adress" } })} text="Insert Sum for Transaction"></input>
                <input value={state.company.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "company" } })} text="Insert Sum for Transaction"></input>
                <input value={state.companyAdress.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "companyAdress" } })} text="Insert Sum for Transaction"></input>
                <input value={state.duty.value} onChange={(e) => dispatch({ type: TEXT_FIELD_CHANGED, payload: { value: e.target.value, ref: "duty" } })} text="Insert Sum for Transaction"></input>
                <input value={state.phoneNumber.value} onChange={(e) => dispatch({ type: PHONE_FIELD_CHANGED, payload: { value: e.target.value, ref: "phoneNumber" } })} text="Insert Sum for Transaction"></input>

                <StyledButton disabled={!state.formIsValid.isValid}>Update Contact</StyledButton>
            </StyledForm>

        </React.Fragment>
    );



}

const StyledButton = styled.button`
    background-color: ${({ disabled }) => (disabled ? "red" : "blue")};
    border-radius:5px;
    height:30px;
    margin-top:10px;
    color:white;
   
`;
const StyledForm = styled.form`
width:50%;
margin: auto;
display: flex;
flex-direction: column;
`;
const StyledFormImg = styled.img`
width:20%;
margin: auto;
display: flex;
flex-direction: column;
`;
export default EditContact